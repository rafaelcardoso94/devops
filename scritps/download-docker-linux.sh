#/bin/bash
echo Instando o docker:
curl -fsSL https://get.docker.com | bash
echo
echo
echo
echo
echo
echo Criando grupo docker:
udo groupadd docker
echo
echo
echo
echo
echo
echo Adicionando o usuario atual no grupo docker:
sudo usermod -aG docker $USER
echo
echo
echo
echo
echo
echo Fazendo logout e login novamente:
newgrp
echo
echo
echo
echo
echo
echo Instalação realizada com sucesso!
