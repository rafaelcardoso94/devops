#/bin/bash
echo Iniciando instalação Terraform:
echo
echo
echo
echo
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version
echo
echo
echo
echo
echo Instalação realizada com sucesso!

